
package com.bvth.pdfformfiller.service;


import com.bvth.pdfformfiller.exception.Errors;
import com.bvth.pdfformfiller.exception.RestException;
import com.google.common.collect.ImmutableMap;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Example to show filling form fields.
 */

@Service
public final class PdfFormFillerServiceImpl implements PdfFormFillerService {

    static final String FIELD_NAME = "fieldName";
    static final String FIELD_VALUE = "fieldValue";

    @Autowired
    PdfTemplateService pdfTemplateService;

    private PdfFormFillerServiceImpl() {
    }

    @Override
    public PDDocument fillForm(Map<String, String> formFields) {

        PDDocument pdfDocument = pdfTemplateService.getPdfTemplateDocument();
        PDAcroForm acroForm = pdfDocument.getDocumentCatalog().getAcroForm();

        formFields.forEach((key, value) -> {
            fillFieldToPdfForm(acroForm, key, value);

        });

        pdfDocument.setAllSecurityToBeRemoved(true);
        return pdfDocument;
    }

    private void fillFieldToPdfForm(PDAcroForm form, String key, String value) {
        PDField field = form.getField(key);
        if (field == null) {
            throw new RestException(Errors.FIELD_NOT_FOUND.toRestError(""));
        }

        try {
            field.setValue(value);
        } catch (Exception e) {
            throw new RestException(Errors.FIELD_VALUE_INVALID.toRestError(e.getMessage()));
        }
    }

}
