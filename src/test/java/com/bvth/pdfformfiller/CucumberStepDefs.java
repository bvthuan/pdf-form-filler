package com.bvth.pdfformfiller;


import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.nio.file.Files;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        PdfFormFillerApplication.class
}, loader = SpringApplicationContextLoader.class)
@WebAppConfiguration
@ActiveProfiles("default")
public class CucumberStepDefs {

    private ResultActions resultActions;
    private volatile MockMvc mockMvc;
    private MockHttpSession mockHttpSession;

    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    private volatile WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockHttpSession = new MockHttpSession();
        this.mockMvc = webAppContextSetup(this.webApplicationContext).build();
    }


    @When("^client request POST to server ([\\S]*) with json file ([\\S]*)")
    public void performPost(String resourceUri, String jsonFile) throws Exception {
        File file = new File("src/test/resources/test-data/" + jsonFile);
        byte[] jsonData = Files.readAllBytes(file.toPath());
        resultActions = perform(post(resourceUri).contentType(MediaType.APPLICATION_JSON)
                .content(jsonData));
    }

    @When("^client request POST to server ([\\S]*) with json data:$")
    public void performPostWithJsonData(String resourceUri, String jsonData) throws Exception {
        resultActions = perform(post(resourceUri).contentType(MediaType.APPLICATION_JSON)
                .content(jsonData.getBytes()));
    }

    @Then("^the response code should be (\\d*)$")
    public void checkResponse(int statusCode) throws Exception {
        resultActions.andExpect(status().is(statusCode));
    }

    @Then("^the response json should be:$")
    public void checkResponse(String json) throws Exception {
        resultActions.andExpect(content().json(json));
    }

    private ResultActions perform(MockHttpServletRequestBuilder request) throws Exception {
        return this.mockMvc.perform(request.session(mockHttpSession));
    }
}
