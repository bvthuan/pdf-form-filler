package com.bvth.pdfformfiller.service;

import com.bvth.pdfformfiller.exception.Errors;
import com.bvth.pdfformfiller.exception.RestException;
import com.google.common.collect.ImmutableMap;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;


@CacheConfig(cacheNames={"pdfTemplate"})
@Service
public class PdfTemplateServiceImpl implements  PdfTemplateService{

    @Autowired
    ResourceLoader resourceLoader;

    public PDDocument getPdfTemplateDocument() {
        try {
            return PDDocument.load(loadPdfTemplateDocumentAndCache());
        } catch (IOException e) {
            throw new RestException(Errors.PDF_TEMPLATE_NOT_LOADED.toRestError(e.getMessage()));
        }
    }

    @Cacheable
    public File loadPdfTemplateDocumentAndCache() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:ESDC-EMP5624.pdf");
        return resource.getFile();
    }
}
