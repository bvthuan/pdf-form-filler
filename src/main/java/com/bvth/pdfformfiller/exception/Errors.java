package com.bvth.pdfformfiller.exception;

import org.springframework.http.HttpStatus;

public enum Errors {

    FIELD_NOT_FOUND(HttpStatus.BAD_REQUEST),
    FIELD_VALUE_INVALID(HttpStatus.BAD_REQUEST),
    PDF_TEMPLATE_NOT_LOADED(HttpStatus.INTERNAL_SERVER_ERROR);

    private HttpStatus httpStatus;

    Errors(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public RestError toRestError(String errorMessage) {
        return new RestError(this.name(), errorMessage, this.httpStatus);
    }
}
