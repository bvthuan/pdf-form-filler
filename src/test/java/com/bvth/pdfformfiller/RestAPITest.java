package com.bvth.pdfformfiller;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/rest/api/integration",
        strict = true,
        format = {"html:target/cucumber-report/restApiIntegration",
                "json:target/cucumber-report/restApiIntegration.json"})
public class RestAPITest {
}
