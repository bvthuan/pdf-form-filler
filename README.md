# pdf-form-filler

Pdf Form Filler

Fill a PDF Document template by receiving  HTTP post request containing a json object that contains pairs of key and value and returns a pdf filled

Currently, the template in this application is Global Talent Stream Application Form(EMP5624). However, this system can be extended to any template document. 

# Tech Stack
1. Spring boot
2. Cucumber 

# Third party
The library used to read and fill pdf document is Apache PDFBox

# What are fields defined in template EMP5624
Reference the file *EMP5624_pdf-fields.txt* for information about field names and types existed in the pdf document EMP5624  
NOTE: PDCheckBox will have two values: "1" or "Off"

# How to run test 
1. Run the command: mvn install test 
2. Test cases defined in cucumber test "PDFFormFillterITTest.feature"
3. Which tests are covered
    1. Successful data set defined in src\test\resources\test-data
    2. Handle invalid field sent to the system 
    3. Handle invalid field value sent to the system. 
4. How to add a new test data.
    1. Add a new json file in folder "src\test\resources\test-data"
    2. Add a new line for the new file in "PDFFormFillterITTest.feature"

# How to test with Postman 
1. Start the server: mvn install spring-boot:run

2. Launch Postman and send requests with this information
    1. Host:  http://localhost:8080/api/fill-form 
    2. Method: POST
    3. Header: Content-Type : application/json
    4. Body: json data. Reference data set defined in src\test\resources\test-data and EMP5624_pdf-fields.txt
    5. Click button "Send and download" instead of "Send". 
    6. Save the file with pdf format.  

