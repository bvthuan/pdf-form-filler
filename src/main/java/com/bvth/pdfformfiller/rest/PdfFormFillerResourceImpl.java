package com.bvth.pdfformfiller.rest;


import com.bvth.pdfformfiller.service.PdfFormFillerService;
import com.google.common.base.Joiner;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@RestController
@RequestMapping("/api/")
public class PdfFormFillerResourceImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfFormFillerResourceImpl.class);

    @Autowired
    PdfFormFillerService pdfFormFillerService;

    @RequestMapping(value = "/fill-form", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<byte[]> fillPdfForm(@RequestBody Map<String, String> formFields) throws IOException {

        LOGGER.info("Received form data from client {} ", Joiner.on(",").withKeyValueSeparator(":").join(formFields));

        PDDocument pdf = pdfFormFillerService.fillForm(formFields);

        return createResponse(pdf);
    }

    private ResponseEntity createResponse(PDDocument pdf) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        pdf.save(byteArrayOutputStream);

        InputStream pdfInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=ESDC-EMP5624_Filled.pdf");

        return new ResponseEntity<byte[]>(IOUtils.toByteArray(pdfInputStream), headers, HttpStatus.OK);
    }

}
