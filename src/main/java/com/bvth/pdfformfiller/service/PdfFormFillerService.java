package com.bvth.pdfformfiller.service;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.util.Map;

public interface PdfFormFillerService {

    PDDocument fillForm( Map<String, String> formFields);

}
