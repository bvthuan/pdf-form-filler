package com.bvth.pdfformfiller.exception;

import com.bvth.pdfformfiller.api.ErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({RestException.class})
    @ResponseBody
    public ResponseEntity handleRestException(RestException ex) {
        ErrorDTO errorDTO = new ErrorDTO(ex.getRestError().getErrorCode(), ex.getRestError().getErrorMessage());

        return new ResponseEntity(errorDTO, ex.getRestError().getHttpStatus());
    }
}
