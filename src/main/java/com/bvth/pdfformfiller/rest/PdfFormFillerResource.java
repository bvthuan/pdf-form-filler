package com.bvth.pdfformfiller.rest;

import org.springframework.http.ResponseEntity;
import java.util.Map;

public interface PdfFormFillerResource {

    ResponseEntity<byte[]> fillPdfForm(Map<String, String> formFields);

}
