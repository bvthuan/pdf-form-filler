package com.bvth.pdfformfiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfFormFillerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfFormFillerApplication.class, args);
	}

}
