package com.bvth.pdfformfiller.service;

import org.apache.pdfbox.pdmodel.PDDocument;

public interface PdfTemplateService {

    PDDocument getPdfTemplateDocument();
}
