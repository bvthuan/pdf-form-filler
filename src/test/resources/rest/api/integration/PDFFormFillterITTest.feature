@restApiIntegration
Feature: PDF Form Filter Integration Test

  Scenario Outline: Fill PDF form successfully
    When client request POST to server /api/fill-form with json file <json_file>
    And the response code should be 200
    Examples:
      | json_file        |
      | test_data_1.json |
      | test_data_2.json |
      | test_data_3.json |

  Scenario: Fill PDF form with unknown field,Should return 400 with error code FIELD_NOT_FOUND
    When client request POST to server /api/fill-form with json data:
    """
      {"unknownField":"1"}
    """
    And the response code should be 400
    And the response json should be:
    """
      {"errorCode":"FIELD_NOT_FOUND"}
    """

  Scenario: Fill PDF form with invalid field, should return 400 with error code FIELD_VALUE_INVALID
    When client request POST to server /api/fill-form with json data:
    """
      {"EMP5624_E[0].Page1[0].Yes_business[0]": "invalidCheckBoxValue"}
    """
    And the response code should be 400
    And the response json should be:
    """
      {"errorCode":"FIELD_VALUE_INVALID"}
    """
