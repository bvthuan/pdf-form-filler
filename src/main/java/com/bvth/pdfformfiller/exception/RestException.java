package com.bvth.pdfformfiller.exception;

public class RestException extends RuntimeException {
    private RestError restError;

    public RestException(RestError restError) {
        this.restError = restError;
    }

    public RestError getRestError() {
        return restError;
    }

    public void setRestError(RestError restError) {
        this.restError = restError;
    }
}
